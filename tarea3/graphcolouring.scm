;; Grupo 1 - Lenguajes de Programacion
;; 1S 2013
;; Anibal Bastias Soto
;; Sebastian Velasquez Catalan
;; Eduardo Hitschfeld Weisser

;Leer archivos
(define (leer archivo)
(let ((p (open-input-file archivo)))
(let leer ((x (read p)))
(if (eof-object? x)
(begin
(close-input-port p)
'())
(cons x (leer (read p)))))))

;Sacar todo lo innecesario y tener solo la matriz de incidencia
(define (purimatriz archivo)
  (purimatriz2 (car(leer archivo)) (cddr(leer archivo))))
  
(define (purimatriz2 x lista)
  (if(= x 0)  
  lista
  (purimatriz2  (- x 1) (cdr lista))))

;; Funcion para obtener una matriz con los colores que se pueden usar como numeros.
(define (armar archivo)
(let construir ((lista (purimatriz archivo)) (x (cadr(leer archivo))) (y 1) (activo 1))
    (cond
      ((null? lista) '() )
     ((and (= activo 1)(= (car lista) 0)) (cons y (construir (cdr lista) x  (+ y 1) 0)))
     ((and (= activo 1)(= (car lista) 1)) (construir (cdr lista) x  (+ y 1) 1))
     ((and (= activo 0) (= y x)) (construir (cdr lista) x 1 1))
      ((and (= activo 0) (< y x)) (construir (cdr lista) x (+ y 1) 0))
      (else lista))))


;; Funcion Check Colors
(define (checkcolors matrix-file)
  (if (>= (car(leer matrix-file)) (mincolors matrix-file)) #t #f))

;; Funcion Min Colors
(define (mincolors archivo)
 (let maximo ((lista (armar archivo))) 
   (if (null? (cdr lista)) (car lista) (if (> (car lista) (maximo(cdr lista) ) ) (car lista) (maximo(cdr lista) )))        
   ))

;; Funcion buscar color
(define  (buscarcolor x lista)
  (let rec ((i 1) (n x) (l lista)) 
    (if(= i n)(car l) (rec (+ i 1) n (cdr l)))
                                    ))
;; Funcion List Colors
(define (listcolors matrix-file)
  (if(eqv? (checkcolors matrix-file) #t)
  (let armado ((l (armar matrix-file)) (matrizcolor (cddr(leer matrix-file)))) 
(if
  (null? (cdr l)) (cons(buscarcolor (car l) matrizcolor) '())    (cons (buscarcolor (car l) matrizcolor) (armado (cdr l) matrizcolor))  
)) '() ))
